# For incremental fixes, update the insignificant version number. For
# a release to CRAN bump the minor version number, for a major change,
# increment the major version number. Each incremental change is
# recorded in $(APP)/inst/NEWS.

APP=rattle
VER=5.0.6

DATE=$(shell date +%Y-%m-%d)
REPOSITORY=repository
RSITELIB=$(shell Rscript -e "cat(installed.packages()['$(APP)','LibPath'])")

help:
	@echo "Manage the $(APP) R package\n\
	============================\n\n\
	rebuild\t\tBuild and install the package.\n\
	  build  \t  Generate $(APP)_$(VER).tar.gz\n\
	  install\t  Install on the local machine\n\
	  test   \t  Start up $(APP) to test it\n\
	  zip    \t  Create a binary zip package\n\n\
	check\t\tCheck for issues with the packaging\n\n\
	dist\t\tUpdate files on /var/www/ and $(APP).togaware.com\n\
	  src    \t  Create src packages (zip and tar).\n\
	  access \t  Copy src to access.togaware.com\n\
	  repo   \t  Update the repository at $(APP).togaware.com.\n\n\
	support\t\tMiscellaneous support functions\n\
	  weather\t  Update the weather datasets from BoM\n\
	  translate\t  Regenerate translations files.\n\
	  home   \t  Update www information\n\n\
	  ucheck \t  Send to Uwe for checking MS/Windows\n\
	  cran   \t  Submit to CRAN or visit http://cran.r-project.org/submit.html\n\n\
	  clean  \t  Clean up.\n\
	  realclean\t  Extra clean.\n\n\
	RSITELIB is $(RSITELIB)\n\
	"

######################################################################
# Include local Makefile

LOCAL_MAKEFILE := $(strip $(wildcard local.mk))

ifneq ($(LOCAL_MAKEFILE),)
    include $(LOCAL_MAKEFILE)
endif

######################################################################
# Include git Makefile

GIT_MAKEFILE := $(strip $(wildcard git.mk))

ifneq ($(GIT_MAKEFILE),)
    include $(GIT_MAKEFILE)
endif

######################################################################
# R Package Management

.PHONY: version
version:
	perl -pi -e 's|^Version: .*|Version: $(VER)|' $(APP)/DESCRIPTION
	perl -pi -e 's|^Date: .*|Date: $(DATE)|' $(APP)/DESCRIPTION
	perl -pi -e 's|^VERSION <- .*|VERSION <- "$(VER)"|' $(APP)/R/$(APP).R
	perl -pi -e 's|^DATE <- .*|DATE <- "$(DATE)"|' $(APP)/R/$(APP).R

.PHONY: check
check: clean version build
	R CMD check --as-cran --check-subdirs=yes $(APP)_$(VER).tar.gz
#	R-devel CMD check --as-cran --check-subdirs=yes $(APP)_$(VER).tar.gz

.PHONY: build
build: version $(APP)_$(VER).tar.gz

.PHONY: rebuild
rebuild: build install

.PHONY: install
install: build
	R CMD INSTALL $(APP)_$(VER).tar.gz

#	scp $(APP)_$(VER).tar.gz adsapub.southeastasia.cloudapp.azure.com:
#	scp $(APP)_$(VER).tar.gz adsapldsvm.southeastasia.cloudapp.azure.com:

.PHONY: test
test: install
	Rscript -e 'library($(APP));$(APP)();Sys.sleep(120)'

$(APP)_$(VER).tar.gz: $(APP)/*
	R CMD build $(APP)

$(APP)_$(VER).check: $(APP)_$(VER).tar.gz
	R CMD check --as-cran --check-subdirs=yes $^

.PHONY: ucheck
ucheck: $(APP)_$(VER).tar.gz
	sh ./upload_uwe.sh
	@echo Wait for email from Uwe Legge.

.PHONY: cran
cran: $(APP)_$(VER).tar.gz
	sh ./upload_cran.sh
	@echo Be sure to email cran@r-project.org.

.PHONY: src
src: $(APP)_$(VER)_src.zip  $(APP)_$(VER)_src.tar.gz

.PHONY: zip
zip: $(APP)_$(VER).zip

$(APP)_$(VER)_src.zip: Makefile $(APP)
	zip -r $@ $^ -x $(APP)/*~ -x $(APP)/*/*~ \
		-x $(APP)/.Rbuildignore -x $(APP)/.git/**\* \
		-x $(APP)/.git/  -x $(APP)/.gitignore \
		-x $(APP)/.Rhistory

$(APP)_$(VER)_src.tar.gz: Makefile $(APP)
	tar zcvf $@ $^ --exclude="*~" --exclude=".git*" --exclude=".R*"

$(APP)_$(VER).zip: install
	(cd $(RSITELIB); zip -r9 - $(APP)) >| $(APP)_$(VER).zip

########################################################################
# Misc

.PHONY: clean
clean:
	@rm -vf $(APP)/*/*~
	@rm -vf $(APP)/*/.Rhistory
	@rm -rf $(APP).Rcheck $(APP).Rcheck

.PHONY: realclean
realclean: clean
	-@mv -v $(APP)_* BACKUP/
	@rm -vf file*xdf

######################################################################
# Knitting

%.tex: %.Rnw
	R -e "knitr::knit('$<')"

%.pdf: %.tex
	rubber --pdf $*

.PHONY: %.view
%.view: %.pdf
	evince $<

%.R: %.Rnw
	R -e "knitr::purl('$<')"

