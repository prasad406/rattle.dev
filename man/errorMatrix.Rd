\name{errorMatrix}
\alias{errorMatrix}
\title{
  Model.
}
\description{
  
  Model.
  
}
\usage{
errorMatrix(model)
}
\arguments{
  
  \item{model}{object.}

}
\references{Package home page: \url{http://rattle.togaware.com}}
\author{\email{Graham.Williams@togaware.com}}

\examples{
  errorMatrix(model)
}
